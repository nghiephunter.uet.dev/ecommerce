FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Ho_Chi_Minh

WORKDIR /var/www/html

RUN apt-get -y update --fix-missing && apt-get upgrade -y

# Install important libraries
RUN apt-get -y install --fix-missing apt-utils build-essential git curl zip wget
RUN apt-get -y install libmcrypt-dev vim software-properties-common

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
ENV SHIPPABLE_NODE_VERSION=16.14.2
RUN . $HOME/.nvm/nvm.sh && nvm install $SHIPPABLE_NODE_VERSION && nvm alias default
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN apt-add-repository ppa:ondrej/php && apt-get update\
&& apt-get -y install --fix-missing php8.0 php8.0-fpm php8.0-mysqli php8.0-curl php8.0-dev \
php8.0-phar php8.0-intl php8.0-ctype php8.0-gearman php8.0-igbinary php8.0-mcrypt \
php8.0-xml php8.0-xmlreader php8.0-xmlrpc \
php8.0-redis php8.0-xmlreader \
php8.0-readline php8.0-soap php8.0-zip \
php8.0-mbstring php8.0-gd php8.0-bcmath php8.0-mbstring \
php8.0-dev php8.0-xml

RUN apt-get -y install nginx

ADD nginx/default.conf /etc/nginx/conf.d/default.conf

RUN rm -rf /etc/nginx/sites-enabled/default

RUN apt-get -y install php-pear
RUN apt-get update && apt-get -y install librdkafka-dev

RUN pecl -d php_suffix=8.0 install rdkafka

COPY . /var/www/html

RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache

RUN chmod -R 777 /var/www/html/storage

EXPOSE 80

CMD service php8.0-fpm start && service nginx start && tail -f /dev/null
