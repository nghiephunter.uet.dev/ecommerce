@extends('admin.home')
@section('content')

<div class="col-md-12">
  <div class="card mb-4">
    <h5 class="card-header">Update User</h5>
    @if (Session::has('message'))
    <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger m-6">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="card-body">
      <form action="{{route('users.update', $user->id)}}" method="post">
        @csrf
        @method('patch')
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Name</label>
          <input id="defaultInput" class="form-control" type="text" placeholder="Name ..." value="{{$user->name}}"
            readonly />
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Email</label>
          <input id="defaultInput" class="form-control" type="email" placeholder="Email ..." value="{{$user->email}}"
            readonly />
        </div>
        <div class="mb-3">
          <label for="defaultSelect" class="form-label">Role</label>
          <select id="defaultSelect" class="form-select" name="role">
            <option value="0">User</option>
            <option value="1">Admin</option>
            <option value="2">Super User</option>
          </select>
        </div>
        <button class="btn btn-primary" type="submit">Update</button>
      </form>
    </div>
  </div>
</div>
@endsection