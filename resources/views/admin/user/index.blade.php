@extends('admin.home')
@section('content')
<!-- Basic Bootstrap Table -->
<div class="card">
  <h5 class="card-header">Users</h5>
  @if (Session::has('message'))
  <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
  @endif
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Role</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody class="table-border-bottom-0">
        @foreach ($users as $user)
        <tr>
          <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{ $user->name}}</strong></td>
          <td>{{ $user->email}}</td>
          <td>{{ $user->role}}</td>
          <td>
            <div style="display: flex; justify-content: center;">
              <a href="{{ route('users.show', $user->id) }}">
                <div class="btn btn-info">Edit</div>
              </a>
              <form method="post" action="{{ route('users.destroy', $user->id) }}">
                @method('delete')
                @csrf
                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
              </form>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $users->withQueryString()->links('pagination::bootstrap-5') !!}
  </div>
</div>
<!--/ Basic Bootstrap Table -->
@endsection
<style>
  tr>th,
  td {
    text-align: center;
  }
</style>