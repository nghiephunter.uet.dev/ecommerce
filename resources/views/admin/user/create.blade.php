@extends('admin.home')
@section('content')

<div class="col-md-12">
  <div class="card mb-4">
    <h5 class="card-header">Create User</h5>
    @if (Session::has('message'))
    <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger m-6">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="card-body">
      <form action="{{route('users.store')}}" method="post">
        @csrf
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Name</label>
          <input id="defaultInput" class="form-control" type="text" placeholder="Name ..." name="name" />
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Email</label>
          <input id="defaultInput" class="form-control" type="email" placeholder="Email ..." name="email" />
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Password</label>
          <input id="defaultInput" class="form-control" type="password" placeholder="Password ..."
            name="password" />
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Confirm Password</label>
          <input id="defaultInput" class="form-control" type="password" placeholder="Confirm Password ..."
            name="c_password" />
        </div>
        <button class="btn btn-primary" type="submit">Create</button>
      </form>
    </div>
  </div>
</div>
@endsection