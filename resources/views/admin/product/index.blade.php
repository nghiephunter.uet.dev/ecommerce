@extends('admin.home')
@section('content')
<!-- Basic Bootstrap Table -->
<div class="card">
  <h5 class="card-header">Products</h5>
  @if (Session::has('message'))
  <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
  @endif
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Description</th>
          <th>Quantity</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody class="table-border-bottom-0">
        @foreach ($products as $product)
        <tr>
          <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{ $product->name}}</strong></td>
          <td>{{ $product->price}} $</td>
          <td>{{ $product->description}}</td>
          <td>{{ $product->quantity_in_stock}}</td>
          <td>
            <div style="display: flex; justify-content: center;">
              <a href="{{ route('products.show', $product->id) }}">
                <div class="btn btn-info">Edit</div>
              </a>
              <form method="post" action="{{ route('products.destroy', $product->id) }}">
                @method('delete')
                @csrf
                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
              </form>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $products->withQueryString()->links('pagination::bootstrap-5') !!}
  </div>
</div>
<!--/ Basic Bootstrap Table -->
@endsection
<style>
  tr>th,
  td {
    text-align: center;
  }
</style>