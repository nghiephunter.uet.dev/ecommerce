@extends('admin.home')
@section('content')

<div class="col-md-12">
  <div class="card mb-4">
    <h5 class="card-header">Create Product</h5>
    @if (Session::has('message'))
    <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger m-6">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="card-body">
      <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Name</label>
          <input id="defaultInput" class="form-control" type="text" placeholder="Name ..." name="name" />
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Price</label>
          <input id="defaultInput" class="form-control" type="number" placeholder="Price ..." name="price" />
        </div>
        <div>
          <label for="exampleFormControlTextarea1" class="form-label">Description</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Description ..." rows="3"
            name="description"></textarea>
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Quantity In Stock</label>
          <input id="defaultInput" class="form-control" type="number" placeholder="Quantity ..."
            name="quantity_in_stock" />
        </div>

        <div class="mb-3">
          <label for="exampleFormControlSelect2" class="form-label">Categories</label>
          <select multiple class="form-select" id="exampleFormControlSelect2" aria-label="Multiple select example"
            name="categories[]">
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
        </div>

        <div class="mb-3">
          <label for="formFileMultiple" class="form-label">Images</label>
          <input class="form-control" type="file" id="formFileMultiple" multiple name="files[]"
            onchange="loadPreview(this);" />
          <img id="preview_img" src="" class="" />
        </div>
        <button class="btn btn-primary" type="submit">Create</button>
      </form>
    </div>
  </div>
</div>
@endsection
<script type="text/javascript">
  function loadPreview(input, id) {
    id = id || '#preview_img';
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(id).attr('src', e.target.result)
                .width(200)
                .height(150);
        };
        reader.readAsDataURL(input.files[0]);
    }
  }
</script>