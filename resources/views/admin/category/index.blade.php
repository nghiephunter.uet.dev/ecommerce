@extends('admin.home')
@section('content')
<!-- Basic Bootstrap Table -->
<div class="card">
  <h5 class="card-header">Categories</h5>
  @if (Session::has('message'))
  <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
  @endif
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody class="table-border-bottom-0">
        @foreach ($categories as $category)
        <tr>
          <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{ $category->name}}</strong></td>
          <td>{{ $category->category_status}}</td>

          <td>
            <div style="display: flex; justify-content: center;">
              <a href="{{ route('categories.show', $category->id) }}">
                <div class="btn btn-info">Edit</div>
              </a>
              <form method="post" action="{{ route('categories.destroy', $category->id) }}">
                @method('delete')
                @csrf
                @if ($category->products == '[]')
                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                @endif
              </form>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $categories->withQueryString()->links('pagination::bootstrap-5') !!}
  </div>
</div>
<!--/ Basic Bootstrap Table -->
@endsection
<style>
  tr>th,
  td {
    text-align: center;
  }
</style>