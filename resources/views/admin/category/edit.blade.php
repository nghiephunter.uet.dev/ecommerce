@extends('admin.home')
@section('content')

<div class="col-md-12">
  <div class="card mb-4">
    <h5 class="card-header">Update Category: {{$category->name}}</h5>
    @if (Session::has('message'))
    <div class="alert alert-primary m-3">{{ Session::get('message') }}</div>
    @endif
    <div class="card-body">
      <form action="{{route('categories.update', $category->id)}}" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Name</label>
          <input id="defaultInput" class="form-control" type="text" placeholder="Name ..."
            value="{{$category->name}}" name="name" />
        </div>
        <div class="mb-3">
          <label for="defaultInput" class="form-label">Status</label>
          <select id="defaultSelect" class="form-select" value="{{$category->category_status}}" name="category_status">
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
        </div>
        <button class="btn btn-primary" type="submit">Update</button>
      </form>
    </div>
  </div>
</div>
@endsection