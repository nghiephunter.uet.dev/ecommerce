<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('category_status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('products_categories', function (Blueprint $table) {
        //     $table->dropForeign('products_categories_category_id_foreign');
        //     $table->dropForeign('products_categories_product_id_foreign');
        //     $table->dropColumn('category_id');
        //     $table->dropColumn('product_id');
        // });
        Schema::dropIfExists('categories');
    }
};
