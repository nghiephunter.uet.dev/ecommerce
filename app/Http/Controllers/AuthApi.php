<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthApi extends Controller
{
    //
    public function getUser()
    {
        return $this->responseData(Auth::user(), 200);
    }

    public function register(Request $request)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'messages' => $validator->errors(),
            ];

            return response()->json($response, 400);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $success['token'] = $user->createToken('MyApp')->plainTextToken;
        $success['name'] = $user->name;

        $response = [
            'success' => true,
            'data' => $success,
            'message' => 'User register successfully',
        ];

        return response()->json($response, 200);
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->plainTextToken;
            $success['name'] = $user->name;

            $response = [
                'success' => true,
                'data' => $success,
                'message' => 'User login successfully',
            ];

            return response()->json($response, 200);
        } else {
            $response = [
                'success' => false,
                'message' => 'Unauthorised',
            ];

            return response()->json($response);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        $response = ['message' => 'logout'];

        return $this->responseData($response, 200);
    }

    public function checkLoggerIn(Request $request)
    {
        return $this->responseData($request->user('sanctum'));
    }

    public function changePass(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|same:password',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);
        if ($validator->fails()) {
            return $this->responseData($validator->errors(), 202);
        } elseif ((Hash::check(request('password'), Auth::user()->password))) {
            DB::table('users')->where('id', '=', Auth::user()->id)->update([
                'password' => bcrypt($request['new_password']),
            ]);

            return $this->responseMessage('success');
        } else {
            return $this->responseMessage('inccorect pass');
        }
    }
}
