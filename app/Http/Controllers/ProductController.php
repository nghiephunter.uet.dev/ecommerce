<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function pushCategory($categories, $product_id)
    {
        $data = [];
        if (! empty($categories)) {
            foreach ($categories as $item) {
                array_push($data, [
                    'product_id' => $product_id,
                    'category_id' => $item,
                ]);
            }
        }

        return $data;
    }

    public function createImage($file, $product_id)
    {
        $filenameWithExt = $file->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        Image::create([
            'product_id' => $product_id,
            'product_img' => 'public/products/'.$fileNameToStore,
        ]);
        $path = $file->storeAs('public/products', $fileNameToStore);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::paginate(5);

        return view('admin.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();

        return view('admin.product.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = new Product();
        $product->fill($request->all());
        $product->save();
        $product ? session()->flash('message', 'Create successfully !')
            : session()->flash('message', 'Create Failed !');
        $data = $this->pushCategory($request->categories, $product->id);
        DB::table('products_categories')->insert($data);
        if ($request->hasfile('files')) {
            foreach ($request->file('files') as $key => $file) {
                $this->createImage($file, $product->id);
            }
        }
        $categories = Category::all();

        return view('admin.product.create', ['categories' => $categories]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $categories = Category::all();
        $product = Product::find($id);

        return view('admin.product.edit', ['product' => $product, 'categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product = Product::find($id)->update([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'quantity_in_stock' => $request->quantity_in_stock,
        ]);
        $images = Image::where('product_id', $id)->get();
        foreach ($images as $image) {
            Storage::delete($image->product_img);
        }
        Image::where('product_id', $id)->delete();
        if ($request->hasfile('files')) {
            foreach ($request->file('files') as $key => $file) {
                $this->createImage($file, $id);
            }
        }
        DB::table('products_categories')->where('product_id', $id)->delete();
        $data = $this->pushCategory($request->categories, $id);
        DB::table('products_categories')->insert($data);
        $product ? session()->flash('message', 'Update successfully !')
            : session()->flash('message', 'Update Failed !');

        return redirect("admin/products/{$id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $images = Image::where('product_id', $id)->get();
            foreach ($images as $image) {
                Storage::delete($image->product_img);
            }
            DB::table('products_categories')->where('product_id', $id)->delete();
            Image::where('product_id', $id)->delete();
            Product::findOrFail($id)->delete();
        } catch (Exception $e) {
            DB::rollback();
            session()->flash('message', 'Delete failed: '.$e->getMessage().' !');

            return redirect('admin/products');
        }
        DB::commit();
        session()->flash('message', 'Delete successfully !');

        return redirect('admin/products');
    }
}
