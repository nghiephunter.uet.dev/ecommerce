<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseData($data)
    {
        return response()->json(['data' => $data]);
    }

    public function responseMessage($message)
    {
        return response()->json(['message' => $message]);
    }

    public function deleteFileStorage($files)
    {
        if (! empty($files)) {
            foreach ($files as $image) {
                Storage::delete($image->product_img);
            }

            return $this->responseMessage('success');
        }

        return $this->responseMessage('failed');
    }
}
