<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'description',
        'quantity_in_stock',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'products_categories');
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
