<?php

use App\Http\Controllers\AuthApi;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', [AuthApi::class, 'getUser']);
    Route::post('/change-pass', [AuthApi::class, 'changePass']);
    Route::post('/logout', [AuthApi::class, 'logout']);
    Route::post('/check', [AuthApi::class, 'checkLoggerIn']);
});

Route::post('/login', [AuthApi::class, 'login']);
Route::post('/register', [AuthApi::class, 'register']);
